import axios from "axios"
import Vue from "vue"

let config = {
    headers: {
        'Access-Control-Allow-Origin': '*',
        //'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE',
        'Access-Control-Allow-Credentials': 'true',
        //'Access-Control-Allow-Headers': 'Access-Control-Allow-Methods, Access-Control-Allow-Origin, Origin, Accept, Content-Type',
        'Content-Type': 'application/json',
        //'client-security-token': 'Iconvers-Token',
        'authorization': 'Iconvers-Token'

    },
};

const api_mixin = {
    data: function () {
        return {
            // eski-baseUrl: http://35.156.177.118/api/
            // yeni-baseUrl: http://3.123.98.249/api
            baseUrl: "http://3.123.98.249"
        }
    },
    methods: {
        api_calling() {
            let service = axios.create();
            //axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
            service.interceptors.response.use(this.handleSuccess());
            return service;
        },
        handleSuccess(response) {
            return response;
        },
        api_get(path, getSuccess, getError) {
            this.api_calling().get(this.baseUrl + path, config).then(
                (response) => (getSuccess(response))
            ).catch(
                (error) => (getError(error))
            );
        },
        api_post(path, payload, postSuccess, postError) {
            this.api_calling().post(this.baseUrl + path, payload, config).then(
                (response) => (postSuccess(response.data))
            ).catch(
                (error) => (postError(error))
            )
        },
        handleAlert(title, text, type) {
            Vue.notify({
                title: title,
                text: text,
                type: type
            });
        }
    }
};

export default api_mixin;