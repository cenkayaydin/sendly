/* eslint-disable no-console */
import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import api_mixin from '../api_mixin'

Vue.use(Vuex);
Vue.mixin(api_mixin);

const moduleApp = {
    state: {
        sidebarXs: false
    }
};

const moduleUser = {
    state: {
        username: null,
        err_log: 0,
        err_second: 0,
        err_min: 0,
        err_count: 0,
    },
    mutations: {
        login(state, data) {
            state.username = data.username
        },
        logout(state) {
            state.username = null;
            this.state.customers.customerList = [];
            this.state.customers.customerType = [];
            this.state.customers.customerList = [];
            this.state.customers.totalCustomer = 0;
            this.state.customers.activeCustomers = 0;
            this.state.customers.passiveCustomers = 0;
            this.state.customers.demoCustomers = 0;
            this.state.customers.paidCustomers = 0;
            this.state.customers.totalCiro = 0;
            this.state.customers.statsCustomerDayDate = [];
            this.state.customers.statsCustomerDayCount = [];
            this.state.customers.totalBills = 0;
        },
        countdown(state) {
            setInterval(function () {
                state.err_second = Math.floor((state.err_count / 1000) % 60);
                state.err_min = Math.floor((state.err_count / 1000) / 60);

                if (state.err_second < 10 && state.err_second >= 0) {
                    state.err_second = "0" + state.err_second
                }
                if (state.err_second < 0) {
                    state.err_second = 59
                }

                if (state.err_second >= 0) {
                    state.err_count -= 1000
                }
            }, 1000);
        }
    }
};

const moduleCustomers = {
    state: {
        customerList: [],
        customerType: [],
        totalCustomer: 0,
        activeCustomers: 0,
        passiveCustomers: 0,
        demoCustomers: 0,
        paidCustomers: 0,
        totalCiro: 0,
        statsCustomerDayDate: [],
        statsCustomerDayCount: [],
        totalBills: 0,
    },
};

const vuexLocal = new createPersistedState({
    storage: window.localStorage
});

const store = new Vuex.Store({
    mixins: [api_mixin],
    modules: {
        user: moduleUser,
        customers: moduleCustomers,
        app: moduleApp,
    },
    plugins: [vuexLocal]
});

export default store
