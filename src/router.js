import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Customers from './views/Customers.vue'
import CreateCustomer from './views/CreateCustomer.vue'
import MemberPackage from "./views/MemberPackage.vue"
import CustomerDetail from "./views/CustomerDetail.vue"
import MemberPackageDetail from "./views/MemberPackageDetail.vue"
import Bills from "./views/Bills.vue"
import NotFound from "./views/404.vue"

Vue.use(Router);

export default new Router({
    //mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                breadCrumb: 'Anasayfa'
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/customers',
            name: 'customers',
            component: Customers,
            meta: {
                breadCrumb: 'Müşteriler'
            },
        },
        {
            path: '/customers/create-customer',
            name: 'createCustomer',
            component: CreateCustomer,
            meta: {
                breadCrumb: 'Müşteri Oluştur'
            },
            parent: Customers
        },
        {
            path: '/membership-packages',
            name: 'membership-packages',
            component: MemberPackage,
            meta: {
                breadCrumb: 'Üyelik Paketleri'
            }
        },
        {
            path: '/customers/customer-detail/:id',
            name: 'customer-detail',
            component: CustomerDetail,
            meta: {
                breadCrumb: 'Müşteri Detayı'
            }
        },
        {
            path: '/customers/package-detail/:id',
            name: 'package-detail',
            component: MemberPackageDetail,
            meta: {
                breadCrumb: 'Paket Detayı'
            }
        },
        {
            path: '/bills',
            name: 'bills',
            component: Bills,
            meta: {
                breadCrumb: 'Faturalar'
            }
        },
        {
            path: '*',
            name: '404',
            component: NotFound
        }
    ]
})
