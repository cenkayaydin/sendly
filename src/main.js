import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

import api_mixin from "../api_mixin.js"

import Notification from "vue-notification"
import moment from "vue-moment"
import moneyFormat from "vue-money-format"
import excel from "vue-excel-export"

Vue.mixin(api_mixin);
Vue.use(Notification);
Vue.use(moment);
Vue.use(moneyFormat);
Vue.use(excel);


export const EventBus = new Vue();

Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function () {
            return EventBus
        }
    }
});


Vue.config.productionTip = false;


new Vue({
    router,
    store,
    axios,
    mixins: [api_mixin],
    render: h => h(App)
}).$mount('#app');
